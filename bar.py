from microbit import *
import random

# version 1.1
glass1 = Image("90009:09090:00900:00900:09990")
glass2 = Image("90009:09590:00900:00900:09990")
glass3 = Image("95559:09590:00900:00900:09990")
animated = [glass1, glass2, glass3, glass2, glass1]

def idle_loop():
  i = 0
  while True:
      display.show(animated, delay=500)
      i += 1
      if random.randint(0,900) == 0:
          display.show(Image.HEART)
          sleep(1000)
      sleep(100)
      if button_a.was_pressed():
          for i in range(5):
              display.show(Image.ALL_CLOCKS, delay=100)
              j = random.randint(0,99)
          while not button_a.was_pressed() and not button_b.was_pressed():
              display.scroll(str(j))
              sleep(100)
      if button_b.was_pressed():
          for i in range(5):
              display.show(Image.ALL_ARROWS, delay=100)
              display.show(random.choice(Image.ALL_ARROWS))
          while not button_a.was_pressed() and not button_b.was_pressed():
              sleep(100)

idle_loop()
